Userscripts
===========

Scripts para mejorar la experiencia de navegación en sitios usados por los
usuarios de Colectron.

Instalación
-----------

### Greasemonkey

Si se utiliza Mozilla Firefox se recomienda la extensión:
<https://addons.mozilla.org/es/firefox/addon/greasemonkey>

### Tampermonkey

Si se utiliza Chromium o Google Chrome se recomienda la extensión:
<http://tampermonkey.net>

1. Click en la extensión Tampermonkey.
2. Click en "Agregar nuevo script...".
3. Pegar el código del *userscript* que se desea utilizar.
4. Click en "Guardar" (o presionar Ctrl+S).
