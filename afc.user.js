// ==UserScript==
// @name        AFC Estudios Jurídicos
// @namespace   https://bitbucket.org/colectron/userscripts
// @description Mejoras en interfaz web para estudios jurídicos de AFC
// @include     https://www.afc.cl/EstJurHome.asp
// @include     https://www.afc.cl/Cobranzas/*
// @include     https://www.afc.cl/EstJurMainFrm.asp
// @grant       none
// @license     GPL-3.0+
// @author      Esteban De La Fuente Rubio, DeLaF (esteban[at]sasco.cl)
// @version     20151025.a
// ==/UserScript==

function AFC()
{
    this.routes = {
        "/estjurhome.asp": "menu",
        "/estjurmainfrm.asp": "main",
        "/cobranzas/paramcalcreaint.asp": "paramcalcreaint",
        "/cobranzas/calcreaint.asp": "calcreaint",
        "/cobranzas/planilladepago.asp": "planilladepago",
    };
}

AFC.prototype.page_menu = function() {
};

AFC.prototype.page_main = function() {
    document.cookie = "AFCAdm=" + getCookie("AFCAdm").replace("&AFPTRABAJOCOBRANZAS=&", "&AFPTRABAJOCOBRANZAS=1008&");
};

AFC.prototype.page_paramcalcreaint = function() {
    var input_rut = document.querySelector(".bktabla3 > td:nth-child(1) > input:nth-child(1)");
    input_rut.setAttribute("maxlength", 12);
    input_rut.setAttribute("size", 12);
    input_rut.setAttribute("onchange", "if (this.value.indexOf('-') !== -1) { document.querySelector('.bktabla3 > td:nth-child(1) > input:nth-child(2)').value = this.value.split('-')[1]; this.value = this.value.split('-')[0]; }");
    input_rut.focus();
};

AFC.prototype.page_calcreaint = function() {
    var form;
    form = document.querySelector("body > form:nth-child(1)");
    form.setAttribute("target", "_blank");
};

AFC.prototype.page_planilladepago = function() {
    var td, input;
    input = document.createElement("input");
    input.setAttribute("type", "checkbox");
    input.setAttribute("name", "afiliados_todos");
    input.setAttribute("onchange", "checkboxes = document.querySelectorAll('input[type=checkbox]'); for (i = 1; i < checkboxes.length; i++) { if (this.checked) checkboxes[i].setAttribute('checked', 'checked'); else checkboxes[i].removeAttribute('checked'); }");
    td = document.querySelector("body > form:nth-child(1) > table:nth-child(26) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > table:nth-child(9) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2)");
    td.appendChild(input);
};

// lanzar script para AFC
bootstrap(new AFC());

/**
 * @link http://www.w3schools.com/js/js_cookies.asp
 */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

/**
 * Función que lanza el script y ejecuta los métodos del mismo según la página
 * que se esté revisado
 * @author Esteban De La Fuente Rubio, DeLaF (esteban[at]delaf.cl)
 * @version 2015-03-19
 */
function bootstrap(script) {
    'use strict';
    var page, method;
    // si hay un bootstrap en el objeto del script se ejecuta
    if (typeof script.bootstrap === "function") {
        script.bootstrap();
    }
    // se ejecuta método del script que está conectado a la página que se está
    // ejecutando
    page = window.location.pathname.toLowerCase();
    if (script.routes[page] !== undefined) {
        method = "page_" + script.routes[page];
        if (typeof script[method] === "function") {
            script[method]();
        } else {
            console.log("Method for " + page + " doesn't exists");
        }
    } else {
        console.log("Route for " + page + " doesn't exists");
    }
}
